import React , {Component, useState} from 'react';
import logo from '../logo.svg';
import '../App.css';
import MapChart from "./MapChart";
import 'bootstrap/dist/css/bootstrap.min.css';
import Astronauts from "./Astronauts";


class App extends Component{
  constructor(props) {
    super(props);
    this.state = {
      coordinates: {lat: 0, lon: 0},
      time: 0
    }

  }

  async componentDidMount() {
    await this.getCoordinates();
    this.interval = setInterval(() => {
      this.getCoordinates();
    }, 5000);
  }

  async getCoordinates(){
    const urlTotal = "http://api.open-notify.org/iss-now.json";
    const responseTotal = await fetch(urlTotal);
    const dataTotal = await responseTotal.json();
    this.setState({
      coordinates: {lat: dataTotal.iss_position.latitude, lon: dataTotal.iss_position.longitude},
      time: dataTotal.timestamp
    })
  }

  render() {
    return (
        <div className="container">
          <Astronauts />
          <MapChart coords={this.state.coordinates} time={this.state.time}/>
        </div>
    );
  }
}

export default App;
