import React, {Component, useState} from "react";
import { Table } from "react-bootstrap";

class Astronauts extends Component{
    constructor(props) {
        super(props);
        this.state = {
            astronauts: []
        }

    }

    async componentDidMount() {
        const urlTotal = "http://api.open-notify.org/astros.json\n";
        const responseTotal = await fetch(urlTotal);
        const dataTotal = await responseTotal.json();
        dataTotal.people.forEach(human => {
            this.setState({
                astronauts: this.state.astronauts.concat(human)
            })
        })
    }



    render() {
        return (
            <div>
                <h1>People in space:</h1>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>Craft</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.astronauts.map(astronaut => (
                        <tr>
                            <td>{astronaut.craft}</td>
                            <td>{astronaut.name}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
        );
    }
}
export default Astronauts